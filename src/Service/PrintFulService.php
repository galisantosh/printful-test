<?php

namespace Santosh\FileCache\Service;


use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;
use Santosh\FileCache\CacheLibrary\CacheInterface;

/**
 * The class implements file cache
 */
class PrintFulService
{

    /**
     * The File cache dependency injection
     */
    public $fileCache;


    /**
     * @param CacheInterface The CacheInterface injection
     */
    public function __construct(CacheInterface $fileCache)
    {
        $this->fileCache = $fileCache;
    }

    /**
     * This function will calculate shipping rates
     * 
     * @param array $requestBody The request body to calculate shipping
     * @return array The calculated shipping rates
     */
    public function calculateShippingRates(array $requestBody) {
        try {
            $client = new Client();
            $url = 'https://api.printful.com/shipping/rates';
            $apiKey = '77qn9aax-qrrm-idki:lnh0-fm2nhmp0yca7';
            if(!$this->fileCache->get('shipping_rates')) {
                $response = $client->request('POST', $url,[
                    'json' => $requestBody,
                    'auth' => [
                        null,
                        $apiKey
                    ],
                ]);
                $data = json_decode($response->getBody(), true);
                $this->fileCache->set('shipping_rates', $data, 300);
            } else {
                $data = $this->fileCache->get('shipping_rates');
            }

            return $data;
        } catch(\Exception $e) {
            return $e->getMessage();
        }
    }
}
