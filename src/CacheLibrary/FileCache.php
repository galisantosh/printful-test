<?php

namespace Santosh\FileCache\CacheLibrary;

use DateInterval;
use function file_exists;
use function gettype;
use function is_int;
use Exception;

/**
 * The class implements CacheInterface
 */
class FileCache implements CacheInterface
{
    /**
     * @var string reserved control characters keys
     */
    const PSR16_RESERVED = '/\{|\}|\(|\)|\/|\\\\|\@|\:/u';

    /**
     * @var string
     */
    private $cachePath;

    /**
     * @var int
     */
    private $dirMode;

    /**
     * @var int
     */
    private $fileMode;

    /**
     * @param string $cachePath  cache folder path
     * @param int    $dirMode    dir permission mode
     * @param int    $fileMode   file permission mode
     */
    public function __construct($cachePath, $dirMode = 0775, $fileMode = 0664)
    {
        $this->dirMode = $dirMode;
        $this->fileMode = $fileMode;
        
        if (!file_exists($cachePath) && file_exists(dirname($cachePath))) {
            $this->mkdir($cachePath); // ensure that the parent path exists
        }

        $path = realpath($cachePath);

        if ($path === false) {
            throw new \Exception("cache path does not exist: {$cachePath}");
        }

        if (!is_writable($path . DIRECTORY_SEPARATOR)) {
            throw new \Exception("cache path is not writable: {$cachePath}");
        }

        $this->cachePath = $path;
    }

    /**
     * Get cache data by key
     * 
     * @param string $key The cache key
     * @return mixed|null cache data
     */
    public function get(string $key)
    {
        $path = $this->getPath($key);

        $expires_at = @filemtime($path);

         // file not found
        if ($expires_at === false)
            return null;

         // if the file expired return false
        if (time() >= $expires_at) {
            @unlink($path);

            return null;
        }

        $data = @file_get_contents($path);

        if ($data === false)
            return null;
        

        if ($data === 'b:0;')
            return null; 

        $value = @unserialize($data);

        if ($value === false)
            return null;

        return $value;
    }

    /**
     * Set the cache data
     * 
     * @param string $key The key to cache data
     * @param mixed $value The value to store in cache
     * @param int $duration The duration of the cache needs to be stored
     */
    public function set(string $key, $value, int $duration)
    {
        $path = $this->getPath($key);

        $dir = dirname($path);

        // check if path exists
        if (!file_exists($dir)) {
            $this->mkdir($dir);
        }

        $temp_path = $this->cachePath . DIRECTORY_SEPARATOR . uniqid('', true);

        if (is_int($duration)) {
            $expires_at = time() + $duration;
        } elseif ($duration instanceof DateInterval) {
            $expires_at = date_create_from_format("U", time())->add($duration)->getTimestamp();
        } elseif ($duration === null) {
            $expires_at = time();
        } else {
            throw new Exception("invalid duration: " . print_r($duration, true));
        }

        if (false === @file_put_contents($temp_path, serialize($value))) {
            return false;
        }

        if (false === @chmod($temp_path, $this->fileMode)) {
            return false;
        }

        if (@touch($temp_path, $expires_at) && @rename($temp_path, $path)) {
            return true;
        }

        @unlink($temp_path);

        return false;
    }

    /**
     * Get the cache path by key
     *
     * @param string $key
     * @return string cache path
     */
    protected function getPath($key)
    {
        $this->validateKey($key);

        $hash = hash("sha256", $key);

        return $this->cachePath
            . DIRECTORY_SEPARATOR
            . strtoupper($hash[0])
            . DIRECTORY_SEPARATOR
            . strtoupper($hash[1])
            . DIRECTORY_SEPARATOR
            . substr($hash, 2);
    }


    /**
     * Validate the path by given key
     * 
     * 
     * @param string $key
     * @throws Exception
     */
    protected function validateKey($key)
    {
        if (!is_string($key)) {
            $type = is_object($key) ? get_class($key) : gettype($key);

            throw new Exception("invalid key type: {$type} given");
        }

        if ($key === "") {
            throw new Exception("invalid key: empty string given");
        }

        if ($key === null) {
            throw new Exception("invalid key: null given");
        }

        if (preg_match(self::PSR16_RESERVED, $key, $match) === 1) {
            throw new Exception("invalid character in key: {$match[0]}");
        }
    }

    /**
     * Recursively create directories
     *
     * @param string $path The path
     */
    private function mkdir($path)
    {
        $parent_path = dirname($path);
        // recursively create dirs
        if (!file_exists($parent_path)) {
            $this->mkdir($parent_path); 
        }

        mkdir($path);
        chmod($path, $this->dirMode);
    }
}
