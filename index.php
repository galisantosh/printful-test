<?php
use Santosh\FileCache\Service\PrintFulService;
use Santosh\FileCache\CacheLibrary\FileCache;

require __DIR__ . '/vendor/autoload.php';

$fileCache = new FileCache("/tmp/fileCache/");
$service = new PrintFulService($fileCache);
$requestBody = [
                "recipient" => [
                    "address1" => "11025 Westlake Dr",
                    "city" => "Charlotte",
                    "country_code" => "NC",
                    "state_code" => "CA",
                    "zip" => 28273
                ],
                "items" => [
                    [
                        "quantity" => 2,
                        "variant_id" => 7679
                    ]
                ]
            ];
var_dump($service->calculateShippingRates($requestBody));